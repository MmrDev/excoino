<?php

namespace Modules\Wallet\Repositories\Order;


use Modules\Wallet\Entities\CurrencyExchange;
use Modules\Wallet\Entities\Order;
use Modules\Wallet\Entities\Wallet;

class OrderRepository implements OrderRepositoryInterface
{

    public function store($data, $userId)
    {

        $price = $this->getPrice($data['to_wallet'], $data['amount']);
        if (!$price){
           return false;
        }
        $orderInstance = Order::create([
            'user_id' => $userId,
            'from_wallet_id' => $this->getWalletByType($data['from_wallet'])->id,
            'to_wallet_id' => $this->getWalletByType($data['to_wallet'])->id,
            'amount' => $data['amount'],
            'final_price' => $price
        ]);
        $md = explode('/', toJalali(date('Y-h-d'))['date']);
        $orderInstance->update([
            'factor_number' => 'Y' . $md[1] . $md[2] . $userId . $orderInstance->id,
            'active' => true
        ]);
        return Order::with('fromWallet', 'toWallet')->find($orderInstance->id);
    }

    public function index($userId)
    {
        $orderCollection = Order::with('fromWallet', 'toWallet', 'user')->where([
            ['user_id', '=', $userId]
        ])->paginate(Request()->get('perPage'));

        if (count($orderCollection)) {
            return $orderCollection;
        }
        return false;
    }

    public function ShowByFactorNumber($factorNumber, $userId)
    {
        $orderInstance = Order::with('fromWallet', 'toWallet', 'user')->where([
            ['factor_number', '=', $factorNumber],
            ['user_id', '=', $userId],
        ])->first();
        if ($orderInstance instanceof Order) {
            return $orderInstance;
        }
        return false;
    }

    public function inactiveOrder($factorNumber, $userId)
    {
        $orderInstance = Order::with('fromWallet', 'toWallet')->where([
            ['factor_number', '=', $factorNumber],
            ['user_id', '=', $userId],
        ])->first();
        if ($orderInstance instanceof Order) {
            $orderInstance->update([
                'active' => false
            ]);
            return $orderInstance;
        }
        return false;
    }

    public function activeOrder($factorNumber, $userId)
    {
        $orderInstance = Order::with('fromWallet', 'toWallet')->where([
            ['factor_number', '=', $factorNumber],
            ['user_id', '=', $userId],
        ])->first();
        if ($orderInstance instanceof Order) {
            $orderInstance->update([
                'active' => true
            ]);
            return $orderInstance;
        }
        return false;
    }


    public function getPrice($toWallet, $price)
    {
        $exchange = Wallet::where([
            ['type', '=', $toWallet],
            ['active', '=', true]
        ])->first();
        if ($exchange instanceof Wallet){
            return $price * $exchange->price;
        }
        return false;
    }

    public function getWalletByType($type)
    {
        return Wallet::where([
            ['type', '=', $type]
        ])->first();
    }
}
