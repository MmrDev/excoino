<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('wallet', [\Modules\Wallet\Http\Controllers\Api\WalletController::class, 'currencyExchange']);

Route::group(['prefix' => 'order'], function () {
    // without Token
    Route::group(['middleware' => 'auth.api'], function () {
        // with Token
        Route::post('', [\Modules\Wallet\Http\Controllers\Api\OrderController::class, 'createOrder'])->middleware('scopes:create');
        Route::get('', [\Modules\Wallet\Http\Controllers\Api\OrderController::class, 'index']);
        Route::get('{factorNumber}', [\Modules\Wallet\Http\Controllers\Api\OrderController::class, 'show']);
        Route::get('inactive/{factorNumber}', [\Modules\Wallet\Http\Controllers\Api\OrderController::class, 'inactiveOrder']);
        Route::get('active/{factorNumber}', [\Modules\Wallet\Http\Controllers\Api\OrderController::class, 'activeOrder']);
    });
});
