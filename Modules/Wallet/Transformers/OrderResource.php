<?php

namespace Modules\Wallet\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);
        return [
            'id' => $data['id'],
            'factor_number' => $data['factor_number'],
            'from_wallet' => $data['from_wallet']['title'],
            'to_wallet' =>$data['to_wallet']['title'],
            'requested_amount' => number_format($data['amount']),
            'price' => number_format($data['final_price']),
            'status' => $data['active'] ? 'active' : 'inactive',
            'created_at' => implode(' - ', toJalali($data['created_at']))
        ];
    }
}
