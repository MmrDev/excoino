<?php


namespace App\Traits;


use App\Models\ConfigAdmin;
use App\Models\InfoLog;
use App\Models\SmsConfig;
use App\Models\User;
use Illuminate\Support\Facades\Http;

trait SendSms
{

    public function otpPattern($number, $code)
    {
        $data = [
            'receptor' => $number,
        ];
        $smsInstance = SmsConfig::where([
            ['type', '=', 'kavenegar'],
            ['active', '=', true],
        ])->first();
        $response = Http::post("https://api.kavenegar.com/v1/https:/api.kavenegar.com/v1/" . $smsInstance->api_key . "/verify/lookup.json?receptor=$number&token=$code&template=verify", $data);
       if ($response->status() === 200){
           InfoLog::create([
               'type' => 'sms',
//               'user_id' => User::where('mobile', $number)->first()->id,
               'subject' => 'پیامک تایید شماره' . ' | '. $number,
               'text' => $code
           ]);
           return response()->json(['data' => $response]);
       }
        InfoLog::create([
            'is_error' => true,
            'type' => 'sms',
//               'user_id' => User::where('mobile', $number)->first()->id,
            'subject' => 'پیامک تایید شماره' . ' | '. $number,
            'text' => $code,
            'error_text' =>$response->toPsrResponse()->getReasonPhrase(),
            'error_code' => $response->status(),
        ]);
       return false;

    }

    public function forgetPassPattern($number, $password)
    {
        $data = [
            'receptor' => $number,
        ];
        $smsInstance = SmsConfig::where([
            ['type', '=', 'kavenegar'],
            ['active', '=', true],
        ])->first();

        $response = Http::post("https://api.kavenegar.com/v1/https:/api.kavenegar.com/v1/" . $smsInstance->api_key . "/verify/lookup.json?receptor=$number&token=$password&template=password", $data);
        if ($response->status() === 200){
            InfoLog::create([
                'type' => 'sms',
                'user_id' => User::where('mobile', $number)->first()->id,
                'subject' => 'پیامک تغییر رمز عبور',
                'text' => $password
            ]);
            return response()->json(['data' => $response]);
        }
        InfoLog::create([
            'is_error' => true,
            'type' => 'sms',
            'user_id' => User::where('mobile', $number)->first()->id,
            'subject' => 'پیامک تغییر رمز عبور',
            'text' => $password,
            'error_text' =>$response->toPsrResponse()->getReasonPhrase(),
            'error_code' => $response->status(),
        ]);
        return false;
    }

    public function otp($number, $code)
    {
        try {
            $smsInstance = SmsConfig::where([
                ['type', '=', 'kavenegar'],
                ['active', '=', true],
            ])->first();
            $message = str_replace('%code%', $code, ConfigAdmin::first()->sms_text);
            $api = new \Kavenegar\KavenegarApi($smsInstance->api_key);
            $sender = $smsInstance->number;
            $result = $api->Send($sender, [$number], $message);

            if ($result) {
                foreach ($result as $r) {
                    InfoLog::create([
                        'type' => 'sms',
                        'user_id' => User::where('mobile', $number)->first()->id,
                        'subject' => 'پیامک تایید شماره',
                        'text' => $message
                    ]);
                    return [
                        'messageId' => $r->messageid,
                        'message' => $r->message,
                        'status' => $r->status,
                        'statusText' => $r->statustext,
                        'sender' => $r->sender,
                        'receptor' => $r->receptor,
                        'date' => $r->date,
                        'cost' => $r->cost,
                    ];
                }
            }
            return $data;
        } catch (\Kavenegar\Exceptions\ApiException $e) {
            // در صورتی که خروجی وب سرویس 200 نباشد این خطا رخ می دهد
            InfoLog::create([
                'is_error' => true,
                'type' => 'sms',
                'user_id' => User::where('mobile', $number)->first()->id,
                'subject' => 'پیامک تایید شماره',
                'text' => $message,
                'error_text' => $e->errorMessage(),
                'error_code' => $e->getCode(),
            ]);
            return $e->errorMessage();
        } catch (\Kavenegar\Exceptions\HttpException $e) {
            // در زمانی که مشکلی در برقرای ارتباط با وب سرویس وجود داشته باشد این خطا رخ می دهد
            InfoLog::create([
                'is_error' => true,
                'type' => 'sms',
                'user_id' => User::where('mobile', $number)->first()->id,
                'subject' => 'پیامک تایید شماره',
                'text' => $message,
                'error_text' => $e->errorMessage(),
                'error_code' => $e->getCode(),
            ]);
            return $e->errorMessage();
        }
    }


    public function activeUser($number)
    {
        $data = [
            'receptor' => $number,
        ];
        $smsInstance = SmsConfig::where([
            ['type', '=', 'kavenegar'],
            ['active', '=', true],
        ])->first();

        $response = Http::post("https://api.kavenegar.com/v1/https:/api.kavenegar.com/v1/" . $smsInstance->api_key . "/verify/lookup.json?receptor=$number&token=فعال&template=active", $data);
        if ($response->status() === 200){
            InfoLog::create([
                'type' => 'sms',
                'user_id' => User::where('mobile', $number)->first()->id,
                'subject' => 'پیامک فعال شدن حساب کاربری',
                'text' => 'حساب کاربری فعال شد',
                'unique_id' => $smsInstance->id
            ]);
            return response()->json(['data' => $response]);
        }
        InfoLog::create([
            'is_error' => true,
            'type' => 'sms',
            'user_id' => User::where('mobile', $number)->first()->id,
            'subject' => 'پیامک فعال شدن حساب کاربری',
            'text' => 'حساب کاربری فعال شد',
            'error_text' =>$response->toPsrResponse()->getReasonPhrase(),
            'error_code' => $response->status(),
            'unique_id' => $smsInstance->id
        ]);
        return false;
    }


    public function sampleSms(array $receptor, $message)
    {
        $GlobKey = 0;
        try {
            $smsInstance = SmsConfig::where([
                ['type', '=', 'kavenegar'],
                ['active', '=', true],
            ])->first();
            $api = new \Kavenegar\KavenegarApi($smsInstance->api_key);
            $sender = $smsInstance->number;
            $result = $api->Send($sender, $receptor, $message);

            $data = [];

            if ($result) {
                foreach ($result as $key => $r) {
                    InfoLog::create([
                        'type' => 'sms',
                        'user_id' => User::where('mobile', $r->receptor)->first()->id,
                        'subject' => 'پیامک تایید شماره',
                        'text' => $r->message,
                        'unique_id' => $smsInstance->id
                    ]);
                    $data[$r->receptor] = [
                        'messageId' => $r->messageid,
                        'message' => $r->message,
                        'status' => $r->status,
                        'statusText' => $r->statustext,
                        'sender' => $r->sender,
                        'receptor' => $r->receptor,
                        'date' => $r->date,
                        'cost' => $r->cost,
                    ];
                    $GlobKey = $key;
                }
            }
            return $data;
        } catch (\Kavenegar\Exceptions\ApiException $e) {
            // در صورتی که خروجی وب سرویس 200 نباشد این خطا رخ می دهد
            InfoLog::create([
                'is_error' => true,
                'type' => 'sms',
                'user_id' => User::where('mobile', $receptor[$GlobKey])->first()->id,
                'subject' => 'پیامک تایید شماره',
                'text' => $message,
                'error_text' => $e->errorMessage(),
                'error_code' => $e->getCode(),
                'unique_id' => $smsInstance->id
            ]);
            return $e->errorMessage();
        } catch (\Kavenegar\Exceptions\HttpException $e) {
            // در زمانی که مشکلی در برقرای ارتباط با وب سرویس وجود داشته باشد این خطا رخ می دهد
            InfoLog::create([
                'is_error' => true,
                'type' => 'sms',
                'user_id' => User::where('mobile', $receptor[$GlobKey])->first()->id,
                'subject' => 'پیامک تایید شماره',
                'text' => $message,
                'error_text' => $e->errorMessage(),
                'error_code' => $e->getCode(),
            ]);
            return $e->errorMessage();
        }
    }
}
