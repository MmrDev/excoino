<?php

namespace Modules\Wallet\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Wallet\Entities\Wallet;

class WalletTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'rials',
                'type' => 'IRR',
                'price' => 1,
                'active' => false
            ], [
                'title' => 'dollar',
                'type' => 'USD',
                'price' => 1,
                'active' => true
            ], [
                'title' => 'euro',
                'type' => 'Eur',
                'price' => 1,
                'active' => true
            ],
        ];
        foreach ($data as $item) {
            Wallet::create($item);
        }
    }
}
