<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends  Authenticatable
{
    use HasFactory;

    protected $guarded = ['id'];


    public function sentMessages()
    {
        return $this->hasMany(AdminMessage::class, 'from_admin_id', 'id')->where('seen', false);
    }

    public function messages()
    {
        return $this->hasMany(AdminMessage::class, 'to_admin_id', 'id');
    }
}
