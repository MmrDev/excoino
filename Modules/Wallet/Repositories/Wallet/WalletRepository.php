<?php

namespace Modules\Wallet\Repositories\Wallet;

use Modules\Wallet\Entities\Wallet;

class WalletRepository implements WalletRepositoryInterface
{

    public function index()
    {
        $exchangeCollection= Wallet::where('active', true)->paginate(Request()->get('perPage'));
        if (count($exchangeCollection)){
            return $exchangeCollection;
        }
        return false;
    }

}
