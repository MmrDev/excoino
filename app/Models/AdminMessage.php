<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminMessage extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function fromAdmin()
    {
        return $this->belongsTo(Admin::class, 'from_admin_id');
    }

    public function toAdmin()
    {
        return $this->belongsTo(Admin::class, 'to_admin_id');
    }
}
