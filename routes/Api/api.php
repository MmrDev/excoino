<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'user'], function () {

    // without Token
    Route::post('login', [AuthController::class, 'login']);

    Route::group(['middleware' => ['auth.api']], function () {

        // with Token
        Route::get('logout', function () {
            Auth::user()->token()->revoke();
        });

        Route::get('profile', [UserController::class, 'profile'])->middleware('scopes:show');


    });

});

