<?php

namespace Modules\Wallet\Http\Controllers\Api;

use App\Traits\Response;
use Illuminate\Routing\Controller;
use Modules\Wallet\Repositories\Wallet\WalletRepositoryInterface;
use Modules\Wallet\Transformers\WalletResource;

class WalletController extends Controller
{
    use Response;

    private $repository;

    public function __construct(WalletRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function currencyExchange()
    {
        $exchangeCollection = $this->repository->index();
        if ($exchangeCollection) {
            $result = $exchangeCollection->toArray();
            $result['data'] = WalletResource::collection($result['data']);
            return $this->errorResponse(200, $result, 200);
        }
        return $this->errorResponse(400, __('errors.no_data'), 400);
    }

}
