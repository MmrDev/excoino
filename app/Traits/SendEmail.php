<?php


namespace App\Traits;


use App\Models\EmailConfig;
use App\Models\InfoLog;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

trait SendEmail
{

    public function email($mail, $code)
    {
        $mailConfig = EmailConfig::where([
            ['active', '=', true]
        ])->first();
        $userId = null;
        $user  = User::where('email', $mail)->first();
        if ($user instanceof User){
            $userId = $user->id;
        }
        try {
            config([
                'mail.mailers.smtp.host' => $mailConfig->host,
                'mail.mailers.smtp.port' => $mailConfig->port,
                'mail.mailers.smtp.encryption' => $mailConfig->encryption,
                'mail.mailers.smtp.username' => $mailConfig->username,
                'mail.mailers.smtp.password' => $mailConfig->password,
            ]);

            $data = [
                'email' => $mail,
                'body' => $code,
                'user' => 'سلام کاربر عزیز',

                'subject' => 'کد تایید ورود به سامانه ifp',
                'from' => 'info@ifp.arad.co',
                'name' => 'arad',
            ];
            Mail::send('mails.otp', $data, function ($message) use ($data) {
                $message->to($data['email'], $data['user'])
                    ->subject($data['subject']);
                $message->from($data['from'], $data['name']);
            });

            InfoLog::create([
                'type' => 'email',
                'user_id' => $userId,
                'subject' => $data['subject'],
                'text' => $data['body'] . ' | ' . $mail,
                'unique_id' => $mailConfig->id
            ]);
            return true;

        } catch (\Exception $exception) {

            InfoLog::create([
                'is_error' => true,
                'type' => 'email',
                'user_id' => $userId,
                'subject' => $data['subject'],
                'text' => $data['body'] . ' | ' . $mail,
                'error_text' => $exception->getMessage(),
                'error_code' => $exception->getCode(),
                'unique_id' => $mailConfig->id
            ]);
            return $exception->getMessage();
        }
    }

    public function activateUserMail($mail)
    {
        $mailConfig = EmailConfig::where([
            ['active', '=', true]
        ])->first();
        $userId = null;
        $user  = User::where('email', $mail)->first();
        if ($user instanceof User){
            $userId = $user->id;
        }
        try {
            config([
                'mail.mailers.smtp.host' => $mailConfig->host,
                'mail.mailers.smtp.port' => $mailConfig->port,
                'mail.mailers.smtp.encryption' => $mailConfig->encryption,
                'mail.mailers.smtp.username' => $mailConfig->username,
                'mail.mailers.smtp.password' => $mailConfig->password,
            ]);

            $data = [
                'email' => $mail,
                'body' => 'حساب کاربری شما فعال شد',
                'user' => 'سلام کاربر عزیز',

                'subject' => 'فعال شدن حساب کاربری',
                'from' => 'info@ifp.arad.co',
                'name' => 'arad',
            ];
            Mail::send('mails.activate', $data, function ($message) use ($data) {
                $message->to($data['email'], $data['user'])
                    ->subject($data['subject']);
                $message->from($data['from'], $data['name']);
            });

            InfoLog::create([
                'type' => 'email',
                'user_id' => $userId,
                'subject' => $data['subject'],
                'text' => $data['body'],
                'unique_id' => $mailConfig->id
            ]);
            return true;

        } catch (\Exception $exception) {

            InfoLog::create([
                'is_error' => true,
                'type' => 'email',
                'user_id' => $userId,
                'subject' => $data['subject'],
                'text' => $data['body'],
                'error_text' => $exception->getMessage(),
                'error_code' => $exception->getCode(),
                'unique_id' => $mailConfig->id
            ]);
            return $exception->getMessage();
        }
    }
}
