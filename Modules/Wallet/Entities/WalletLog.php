<?php

namespace Modules\Wallet\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class WalletLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'wallet_id',
        'price'
    ];

    protected static function newFactory()
    {
        return \Modules\Wallet\Database\factories\WalletLogFactory::new();
    }
}
