<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\PostRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{


    public function profile()
    {
        return $this->successResponse(200, Auth::user(), 200);
    }
}
