<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class HandlePaginate
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->has('perPage')){
            $request['perPage'] = $request->input('perPage') > 0 ? (integer)$request->input('perPage') : 15;
        }else{
            $request['perPage'] = 15;
        }
        return $next($request);
    }

}
