<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Wallet\Repositories\Order\OrderRepositoryInterface;
use Modules\Wallet\Repositories\Order\OrderRepository;
use Modules\Wallet\Repositories\Wallet\WalletRepository;
use Modules\Wallet\Repositories\Wallet\WalletRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(OrderRepositoryInterface::class, OrderRepository::class);
        $this->app->bind(WalletRepositoryInterface::class, WalletRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
