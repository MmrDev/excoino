<?php

namespace Modules\Wallet\Http\Controllers\Api;

use App\Traits\Response;
use Illuminate\Routing\Controller;
use Modules\Wallet\Http\Requests\CreateOrderRequest;
use Modules\Wallet\Repositories\Order\OrderRepositoryInterface;
use Modules\Wallet\Transformers\OrderResource;

class OrderController extends Controller
{
    use Response;
    private $repository;

    public function __construct(OrderRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }


    public function index()
    {
        $orderCollection = $this->repository->index(auth()->id());
        if ($orderCollection){
            $orderCollection = $orderCollection->toArray();
            $orderCollection['data'] = OrderResource::collection($orderCollection['data']);
            return $this->successResponse(200, $orderCollection , 200);
        }
        return $this->errorResponse(400, __('errors.no_data'), 400);
    }


    public function show($factorNumber)
    {
        $orderInstance = $this->repository->ShowByFactorNumber($factorNumber,auth()->id());
        if ($orderInstance){
            return $this->successResponse(200, new OrderResource($orderInstance), 200);
        }
        return $this->errorResponse(400, __('errors.no_data'), 400);
    }


    public function createOrder(CreateOrderRequest $request)
    {
        $orderInstance = $this->repository->store($request->validated(),auth()->id());
        if ($orderInstance){
            return $this->successResponse(200, new OrderResource($orderInstance), 200);
        }
        return $this->errorResponse(400, __('errors.try_again'), 400);
    }


    public function inactiveOrder($factorNumber)
    {
        $orderInstance = $this->repository->inactiveOrder($factorNumber,auth()->id());
        if ($orderInstance){
            return $this->successResponse(200, new OrderResource($orderInstance), 200);
        }
        return $this->errorResponse(400, __('errors.no_data'), 400);
    }

    public function activeOrder($factorNumber)
    {
        $orderInstance = $this->repository->activeOrder($factorNumber,auth()->id());
        if ($orderInstance){
            return $this->successResponse(200, new OrderResource($orderInstance), 200);
        }
        return $this->errorResponse(400, __('errors.no_data'), 400);
    }


}
