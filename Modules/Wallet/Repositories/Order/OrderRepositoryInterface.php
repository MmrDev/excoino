<?php

namespace Modules\Wallet\Repositories\Order;

interface OrderRepositoryInterface
{

    public function store(array $data, $userId);

    public function index($userId);

    public function ShowByFactorNumber(string $factorNumber, $userId);

    public function inactiveOrder(string $factorNumber, $userId);

    public function activeOrder(string $factorNumber, $userId);

}
