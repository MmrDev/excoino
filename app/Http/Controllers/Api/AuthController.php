<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $data = $this->checkMail($request->validated());

        if ($data) {
            $userInstance = $data['userInstance'];
            unset($data['userInstance']);
            if (Auth::attempt($data)) {
                Auth::login($userInstance);
                if ($userInstance->type == 'member') {
                    $token = Auth::user()->createToken('full user'
                        , ['create',
                            'update',
                            'show',
                            'delete']);
                } else {
                    $token = Auth::user()->createToken('guest user'
                        , [
                            'update',
                            'show',
                            'delete'
                        ]);
                }

                $cookie = $this->getCookieDetails($token->accessToken);
                return $this->successResponse('user_login', [
                    'userInstance' => $userInstance,
                    'token' => $token->accessToken
                ], 200)
                    ->cookie(
                        $cookie['name'],
                        $cookie['value'], $cookie['minutes'],
                        $cookie['path'], $cookie['domain'],
                        $cookie['secure'], $cookie['httpOnly']
                    );
            }
            return $this->errorResponse(400, __('errors.wrong_username_password'), 400);
        }
        return $this->errorResponse(400, __('errors.user_not_exists'), 400);
    }



    //*** *** Helper *** ***//

    private function checkMail($data)
    {
        $find1 = strpos($data['username'], '@');
        if ($find1) {
            if (strpos($data['username'], '.')) {
                $userInstance = User::where('email', $data['username'])->first();
                if ($userInstance instanceof User) {
                    return [
                        'email' => $data['username'],
                        'password' => $data['password'],
                        'userInstance' => $userInstance
                    ];
                }
                return false;
            };
        }
        $userInstance = User::where('username', $data['username'])->first();
        if ($userInstance instanceof User) {
            return [
                'email' => $data['username'],
                'password' => $data['password'],
                'userInstance' => $userInstance
            ];
        }
        return false;
    }

    private function getCookieDetails($token)
    {
        return [
            'name' => 'token',
            'value' => $token,
            'minutes' => 1440,
            'path' => null,
            'domain' => null,
            // 'secure' => true, // for production
            'secure' => null, // for localhost
            'httpOnly' => true,
        ];
    }

}
