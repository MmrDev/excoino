<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Http;
use Modules\Wallet\Entities\Wallet;
use Modules\Wallet\Entities\WalletLog;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     *
     */
    protected function schedule(Schedule $schedule)
    {
        $data = [];
        $schedule->call(function () use ($data) {
            $response = Http::get('http://api.navasan.tech/latest/?api_key=BEtNOwk43ITmZ7iO9iNRV1TxeYejjket');
            $data[] =
                [
                    'name' => 'USD',
                    'amount' => $response['usd']['value'] * 10
                ];
            $data[] =
                [
                    'name' => 'Eur',
                    'amount' => $response['eur']['value'] * 10
                ];
            foreach ($data as $item) {
                $walletInstance = Wallet::where('type', $item['name'])->first();
                $walletInstance->update([
                    'price' => $item['amount']
                ]);
               WalletLog::create([
                  'wallet_id'=> $walletInstance->id,
                   'price' => $item['amount']
               ]);
            }
        })->everyMinute();
    }


    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

}
