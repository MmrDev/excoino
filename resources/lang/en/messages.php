<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Success Messages
    |--------------------------------------------------------------------------
    */

    '200' => 'everything is ok',
    'user_login' => 'you are logged in successfully, enjoy :D',
    'register_successfully' => 'you are registered successfully, you can login with you information',
    'deleted_successfully' => 'post has been deleted successfully'


];
